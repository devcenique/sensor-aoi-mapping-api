import * as fromDeviceGroup from './deviceGroup.service';
import * as fromDevice from './device.service';
import * as fromAccount from './account.service';
import * as fromPOI from './poi.service';

export const getStoresFromAccountIds = async (accountIds?: string[]) => {
    const stores: any[] = [];
    const accounts = await fromAccount.readAll(
        accountIds ? { _id: accountIds } : {}
    );
    const deviceGroups = await fromDeviceGroup.readAll(
        accountIds ? { account_id: accountIds } : {}
    );
    const devices = await fromDevice.readAll(
        accountIds
            ? {
                  accountId: accountIds,
                  mac: {
                      $nin: [
                          'AA:BB:CC:DD:EE:FF',
                          '00:11:22:33:44:55',
                          '00:00:00:00:00:01'
                      ]
                  }
              }
            : {
                  mac: {
                      $nin: [
                          'AA:BB:CC:DD:EE:FF',
                          '00:11:22:33:44:55',
                          '00:00:00:00:00:01'
                      ]
                  }
              }
    );
    const records = await fromPOI.readAll({});
    deviceGroups.forEach(deviceGroup => {
        const account = accounts.find(
            a => a._id.toString() === deviceGroup.account_id
        );
        if (account) {
            stores.push({
                id: deviceGroup._id.toString(),
                name: deviceGroup.name,
                groupId: deviceGroup._id.toString(),
                accountId: deviceGroup.account_id,
                company: account.company
                    ? account.company
                    : account.contact_person,
                devices: []
            });
        }
    });
    devices.forEach(device => {
        let edgeDevice,
            name = '',
            sensorCode = '';
        if (device.mac.includes('_')) {
            edgeDevice = devices.find(d => d.mac === device.mac.split('_')[0]);
            if (edgeDevice) {
                device.deviceGroupId = edgeDevice.deviceGroupId;
            }
        }
        if (device.virtualDeviceId) {
            const record = records.find(r => r.vid === device.virtualDeviceId);
            if (record) {
                name = record.name;
                sensorCode = record.poi;
            }
        }
        const store = stores.find(s => s.groupId === device.deviceGroupId);
        if (store) {
            store.devices.push({
                name,
                sensorCode,
                deviceType: '',
                sensorType: device.type ? device.type : '',
                inputType: '',
                mac: edgeDevice ? edgeDevice.mac : device.mac,
                channelName: device.name,
                channelInput: device.mac,
                description: device.description ? device.description : '',
                vid: device.virtualDeviceId ? device.virtualDeviceId : undefined
            });
        }
    });
    return stores;
};

export const readAll = async () => {
    return await getStoresFromAccountIds();
};
