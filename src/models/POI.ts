import * as mongoose from 'mongoose';

import { adConn } from '@app/connections/mongo';

export interface POI {
    poi: string;
    name: string;
    description: string;
    customerId: string;
    vid: string;
}

// const Schema = mongoose.Schema;

const schema = new mongoose.Schema(
    {
        poi: String,
        name: String,
        description: String,
        customerId: String,
        vid: String
    },
    {
        collection: 'poi'
    }
);

export const POIModel = adConn.model('poi', schema);
export default POIModel;
