import { Request, Response } from 'express';
import * as _ from 'lodash';

import * as RoiController from './poi.service';

export const createNewPoi = async (req: Request, res: Response) => {
    try {
        const { poi, name, description, customerId, vid } = req.body;
        res.send(
            await RoiController.create(poi, name, description, customerId, vid)
        );
    } catch (err) {
        res.status(400).send(err);
    }
};

export const readAllPoi = async (req: Request, res: Response) => {
    try {
        if (req.query.vid !== undefined) {
            req.query.vid = req.query.vid.split(',');
        }
        res.send(await RoiController.readAll(req.query));
    } catch (err) {
        res.status(400).send(err);
    }
};

export const readPoi = async (req: Request, res: Response) => {
    try {
        res.send(await RoiController.read(req.params.id));
    } catch (err) {
        res.status(400).send(err);
    }
};

export const updateManyPoi = async (req: Request, res: Response) => {
    try {
        const bulks = req.body.poi.map((element: any) => {
            return {
                updateOne: {
                    filter: { _id: element._id },
                    update: element
                    // upsert: true
                }
            };
        });
        res.send(await RoiController.bulkWrite(bulks));
    } catch (err) {
        res.status(400).send(err);
    }
};

export const updatePoi = async (req: Request, res: Response) => {
    try {
        res.send(await RoiController.update(req.params.id, req.body));
    } catch (err) {
        res.status(400).send(err);
    }
};

export const deleteManyPoi = async (req: Request, res: Response) => {
    try {
        const bulks = req.body.ids.map((_id: any) => ({
            deleteOne: {
                filter: { _id }
            }
        }));
        res.send(await RoiController.bulkWrite(bulks));
    } catch (err) {
        res.status(400).send(err);
    }
};

export const deletePoi = async (req: Request, res: Response) => {
    try {
        res.send(await RoiController.destory(req.params.id));
    } catch (err) {
        res.status(400).send(err);
    }
};
