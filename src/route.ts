import { Express } from 'express';

import * as ApiController from '@app/controllers/api';
import * as StoresController from '@app/controllers/stores';

const route = '/api';

module.exports = (app: Express) => {
    app.post(`${route}/poi`, ApiController.createNewPoi);

    app.get(`${route}/poi`, ApiController.readAllPoi);

    app.get(`${route}/poi/:id`, ApiController.readPoi);

    app.patch(`${route}/poi`, ApiController.updateManyPoi);

    app.patch(`${route}/poi/:id`, ApiController.updatePoi);

    app.delete(`${route}/poi`, ApiController.deleteManyPoi);

    app.delete(`${route}/poi/:id`, ApiController.deletePoi);

    app.get(`${route}/stores`, StoresController.readAll);
};
