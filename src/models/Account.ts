import * as mongoose from 'mongoose';

import { avaConn } from '@app/connections/mongo';

export class AvaAccount {
    _id: mongoose.Schema.Types.ObjectId;
    resellerId?: string;
    contact_person?: string;
    company?: string;
}

const schema = new mongoose.Schema(
    {
        _id: mongoose.Schema.Types.ObjectId,
        resellerId: String,
        contact_person: String,
        company: String
    },
    { collection: 'app_account' }
);

export const AvaAccountModel = avaConn.model('AvaAccount', schema);
export default AvaAccountModel;
