import { AvaDeviceModel } from '@app/models/Device';

export const readAll = async (query: any) => {
    const result = await AvaDeviceModel.find(query);
    return result.map(r => {
        return r.toObject();
    });
};
