import { AvaAccountModel } from '@app/models/Account';

export const readAll = async (query: any) => {
    const result = await AvaAccountModel.find(query);
    return result.map(r => {
        return r.toObject();
    });
};
