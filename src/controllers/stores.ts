import { Request, Response } from 'express';
import * as _ from 'lodash';

import * as fromStores from './stores.service';

export const readAll = async (req: Request, res: Response) => {
    try {
        res.send(await fromStores.readAll());
    } catch (err) {
        res.status(400).send(err);
    }
};
