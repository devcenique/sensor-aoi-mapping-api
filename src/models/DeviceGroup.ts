import * as mongoose from 'mongoose';

import { avaConn } from '@app/connections/mongo';
import { AvaDevice } from './Device';

export class AvaDeviceGroup {
    _id: mongoose.Schema.Types.ObjectId;
    name: string;
    account_id: string;
    devices: AvaDevice[];
}

const schema = new mongoose.Schema(
    {
        name: String,
        account_id: String
    },
    {
        collection: 'deviceGroup',
        toJSON: {
            virtuals: true
        },
        toObject: {
            virtuals: true
        }
    }
);

export const AvaDeviceGroupModel = avaConn.model('AvaDeviceGroup', schema);
export default AvaDeviceGroupModel;
