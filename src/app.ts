import { EventEmitter } from 'events';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';

import { connect as mongoConnect } from '@app/connections/mongo';

EventEmitter.prototype.setMaxListeners(100);

const app = express();
app.set('port', 3000);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

export async function connect() {
    await mongoConnect();
}

export default app;
