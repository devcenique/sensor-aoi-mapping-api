import { AvaDeviceGroupModel } from '@app/models/DeviceGroup';

export const readAll = async (query: any) => {
    const result = await AvaDeviceGroupModel.find(query);
    return result.map(r => {
        return r.toObject();
    });
};
