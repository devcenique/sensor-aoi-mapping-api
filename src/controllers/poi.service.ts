import { POIModel } from '@app/models/POI';

export const create = async (
    poi: string,
    name: string,
    description: string,
    customerId: string,
    vid: string
) => {
    const newModel = new POIModel({
        poi,
        name,
        description,
        customerId,
        vid
    });
    const obj = (await newModel.save()).toObject();
    return obj;
};

export const readAll = async (query: any) => {
    const result = await POIModel.find(query);
    return result.map(r => {
        return r.toObject();
    });
};

export const read = async (id: string) => {
    const result = await POIModel.findById(id);
    return result.toObject();
};

export const update = async (id: string, query: any) => {
    const result = await POIModel.findByIdAndUpdate(id, query, { new: true });
    return result.toObject();
};

export const destory = async (id: string) => {
    await POIModel.findByIdAndDelete(id);
    return 'Delete Successful';
};

export const bulkWrite = async (bulks: any[]) => {
    return await POIModel.bulkWrite(bulks);
};
