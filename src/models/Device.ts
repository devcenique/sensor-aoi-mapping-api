import * as mongoose from 'mongoose';
import { avaConn } from '@app/connections/mongo';

export class AvaDevice {
    _id: mongoose.Types.ObjectId;
    status: string;
    accountId: string;
    group: string;
    mac: string;
    name: string;
    description?: string;
    type: string;
    virtualDeviceId?: string;
    deviceGroupId: string;
}

const schema = new mongoose.Schema(
    {
        status: String,
        accountId: String,
        group: String,
        mac: String,
        name: String,
        description: String,
        type: String,
        virtualDeviceId: String,
        deviceGroupId: String
    },
    {
        collection: 'device'
    }
);

export const AvaDeviceModel = avaConn.model('AvaDevice', schema);
export default AvaDeviceModel;
