export const environment = {
    mongo: {
        ava:
            'mongodb://cnq0512:cnq0512@prod-inreality-shard-00-00-65rnq.mongodb.net:27017,prod-inreality-shard-00-01-65rnq.mongodb.net:27017,prod-inreality-shard-00-02-65rnq.mongodb.net:27017/ava-server-prod?ssl=true&replicaSet=prod-inreality-shard-0&authSource=admin',
        ad: 'mongodb://127.0.0.1/ad-server'
    }
};
