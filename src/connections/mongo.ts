import * as mongoose from 'mongoose';
import { Connection } from 'mongoose';

import { environment } from '@env/environment';

export let adConn: Connection;
export let avaConn: Connection;

export const connect = async () => {
    adConn = mongoose.createConnection(environment.mongo.ad);
    avaConn = mongoose.createConnection(environment.mongo.ava);
};
